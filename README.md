# React + Redux Tutorial

## Setup
1. `npm install`
2. `gulp`

## Extra Reading
- [React Docs](https://facebook.github.io/react/docs/reusable-components.html)
- [Babel & ES6](https://babeljs.io/docs/learn-es2015/)
- [Seperation of Concerns principle](http://deviq.com/separation-of-concerns/)
- [Redux](http://redux.js.org/)
- [React Stamp](https://github.com/stampit-org/react-stamp)

## React Topics
- Components
- Props + Virtual DOM + Declarative-ness
- State
- JSX
- react stamp
- composing render output

## Redux Topics
- actions
- reducers
- store
- react-redux: select + connect + provider

## Task 1
* Create a calculator app
  * it should be split up into at least the following components: Calculator, Button, Display.
  * Calculator component does all data processing and storage, the other components are "stupid" (ie. dont know anything about state - only receive data and callback methods via props).
  * It should be able to add, subtract, multiply and divide.
  * Try use some ES6/7 features.
  * The rest of the details are up to you.
