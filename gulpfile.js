require("babel-register")({
  presets: ["es2015", "react", "stage-0"]
});

var path = require('path');
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babelify = require('babelify');
var livereactload = require('livereactload');
var source = require('vinyl-source-stream');
var jade = require('gulp-jade');
var del = require('del');
var eslint = require('gulp-eslint');

var PATHS = {
  WEB_CLIENT_SRC: 'webclient_src',
  WEB_CLIENT_DIST: 'webclient_dist',
};

gulp.task('clean', function(done) {
  del.sync(PATHS.WEB_CLIENT_DIST);
  done();
});

gulp.task('connect', function(done) {
  var express = require('express');
  var path = require('path');
  var port = 5555;
  var app = express();

  app.use('', express.static(PATHS.WEB_CLIENT_DIST));
  app.get('*', function (req, res){
    res.sendFile(path.resolve(__dirname, PATHS.WEB_CLIENT_DIST + '/index.html'));
  })
  app.listen(port);
  console.log("server started on port " + port);
});

gulp.task('jade', function(done){
  gulp
    .src(path.join(PATHS.WEB_CLIENT_SRC, 'index.jade'))
    .pipe(jade({pretty: true}))
    .pipe(gulp.dest(PATHS.WEB_CLIENT_DIST))
    .on('end', done);
});

gulp.task('js', function(done) {
  var bify = browserify(path.join(PATHS.WEB_CLIENT_SRC, 'main.js'), {
      debug: false,
    })
    .transform(babelify);

  bify.plugin(livereactload);
  var bundler = watchify(bify);
  bundler.on('update', function() {
    console.log('-> bundling...');
    rebundle();
  });

  function rebundle() {
    return bundler.bundle()
      .on('error', function(err) { console.error(err); this.emit('end'); })
      .pipe(source('bundle.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(path.join(PATHS.WEB_CLIENT_DIST, 'js')));
  }

  return rebundle();
});

gulp.task('lint', function () {
  return gulp.src([path.join(PATHS.WEB_CLIENT_SRC, '/**/*.js'), '!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('watch', function() {
	gulp.watch(path.join(PATHS.WEB_CLIENT_SRC, 'index.jade'), ['jade']);
	gulp.watch(path.join(PATHS.WEB_CLIENT_SRC, '**/*.js'), ['lint']);
});

gulp.task('default', [
  'clean',
  'lint',
  'jade',
  'js',
  'watch',
  'connect',
]);
