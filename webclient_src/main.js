import React from 'react';
import ReactDOM from 'react-dom';
import stamp from 'react-stamp';
import _ from 'lodash';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import { apiMiddleware } from 'redux-api-middleware';
import AppService from './appService.js';

// action types that can be triggered on the store
const ACTION_TYPES = {
  ADD_ITEM: 'ADD_ITEM',
  REMOVE_ITEM: 'REMOVE_ITEM',
  ADD_PERSON: 'ADD_PERSON',
  REMOVE_PERSON: 'REMOVE_PERSON',
  GET_ALL_ITEMS_REQUEST: 'GET_ALL_ITEMS_REQUEST',
  GET_ALL_ITEMS_SUCCESS: 'GET_ALL_ITEMS_SUCCESS',
  GET_ALL_ITEMS_FAILURE: 'GET_ALL_ITEMS_FAILURE',
};

// reducer function that will be triggered by the store every time an action is triggered
// this one is for items
const itemsReducer = (state = [], action) => {
  // action = {type: .., payload: ...}
  // create copy of state because cannot alter original state
  let newState = _.cloneDeep(state);
  switch(action.type) {
    case ACTION_TYPES.ADD_ITEM:
      newState.push(action.payload);
      return newState;
    case ACTION_TYPES.REMOVE_ITEM:
      newState.pop();
      return newState;
    // case ACTION_TYPES.GET_ALL_ITEMS_SUCCESS:
    // case ACTION_TYPES.GET_ALL_ITEMS_FAILURE:
    case ACTION_TYPES.GET_ITEM_REQUEST:
      newState.push(action.payload.data);
      return newState;
    case ACTION_TYPES.GET_ALL_ITEMS_REQUEST:
      action.payload = {
        data: [
          'item 1',
          'item 2',
        ]
      };
      return action.payload.data;
    default:
      return state;
  }
};

// this one is for people
const peopleReducer = (state = [], action) => {
  let newState = _.cloneDeep(state);
  switch(action.type) {
    case ACTION_TYPES.ADD_PERSON:
      newState.push(action.payload);
      return newState;
    case ACTION_TYPES.REMOVE_PERSON:
      newState.pop();
      return newState;
    default:
      return state;
  }
};

// store for keeping our app's global state as a json object
// combine reducers into one
// apply middleware that intercepts each action that gets triggered and checks if it should send an ajax request
let store = applyMiddleware(
  apiMiddleware
)(createStore)(combineReducers({
  items: itemsReducer,
  people: peopleReducer,
}));
// state/store now looks something like this
// {
//   items: [],
//   people: [],
// }

// method that is used to select the parts of the global state that will be passed to a component as props
const select = (state) => state;

// App component that has been connected to the store uisung the selector fn above
const App = connect(select)(stamp(React)
  .compose({
    componentDidMount() {
      setTimeout(() => {
        this.props.dispatch(AppService.getAllItems());
      }, 3000)
    },

    handleAddItem(id) {
      // this.props.dispatch(AppService.getItem(id));
      this.props.dispatch({
        type: ACTION_TYPES.ADD_ITEM,
        payload: 'item',
      });
    },

    handleRemoveItem() {
      this.props.dispatch({
        type: ACTION_TYPES.REMOVE_ITEM,
      });
    },

    handleAddPerson() {
      this.props.dispatch({
        type: ACTION_TYPES.ADD_PERSON,
        payload: 'person',
      });
    },

    handleRemovePerson() {
      this.props.dispatch({
        type: ACTION_TYPES.REMOVE_PERSON,
      });
    },

    render() {
      let items = this.props.items.map((item, i) => {
        return <li key={i}>{item}</li>;
      });

      let people = this.props.people.map((item, i) => {
        return <li key={i}>{item}</li>;
      });

      return (
        <div>
          <a onClick={this.handleAddItem.bind(this)}>Add</a> |
          <a onClick={this.handleRemoveItem.bind(this)}>Remove</a>
          <a onClick={this.handleAddPerson.bind(this)}>Add Person</a> |
          <a onClick={this.handleRemovePerson.bind(this)}>Remove Person</a>
          <ul>
            {items}
          </ul>
          <ul>
            {people}
          </ul>
        </div>
      );
    }
  }))

const div = document.getElementById('main');
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
, div);
