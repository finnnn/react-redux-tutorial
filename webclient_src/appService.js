import { CALL_API } from 'redux-api-middleware';

// service class that has methods for triggering actions that send ajax requests
class AppService {
  static getAllItems() {
    return {
      [CALL_API]: {
        method: 'GET',
        endpoint: 'http://example.com',
        headers: {},
        types: [
          'GET_ALL_ITEMS_REQUEST',
          'GET_ALL_ITEMS_SUCCESS',
          'GET_ALL_ITEMS_FAILURE'
        ],
      }
    }
  }
}

export default AppService;
